# single-cell-preprocessing
A snakemake pipeline to preprocess data from single cell RNAseq. `sc-preprocess` can handle data from multiple platforms, e.g. C1, Wafergen and DropSeq. Documentation is located here:  [link](https://gitlab.gwdg.de/loosolab/software/scRNAseq.preprocessing/-/blob/master/book/toc.md)

## Setup
```
git clone ...
cd single-cell-preprocessing
conda env create -f environment.yml
conda activate snakemake
```

-------------------------
## Quickstart for DropSeq-based data
#### Configuration
Edit the file `config/dropseq.yml` and adapt the options to your needs. Pay attention to the `data` section and add the paths to individual fastq files like this:

```
data:
  files:
    condition1:
      r1: path/to/condition1_r1.fastq.gz
      r2: path/to/condition1_r2.fastq.gz
    condition2:
      r1: path/to/condition2_r1.fastq.gz
      r2: path/to/condition2_r2.fastq.gz
```

#### Run analysis
A call to `snakemake --configfile <config> --use-conda` will create a whitelist of true barcodes per condition, demultiplex barcodes into fastq files for each valid barcode and quantify the data.

-----------------------------------
## Quickstart for plate-based data (C1, Wafergen)
#### Configuration
Edit the file `config/plateseq.yml` and adapt the options to your needs. If you already have fastq files for each cell (sample), set `demultiplex: False` in the `action` section. 

#### Writing the samplesheet
Plate-based data needs a samplesheet, that gives information for each sample that should be processed. A samplesheet is a tab-separated table that contains *at least* a column with a samples name:

| Name  | Batch | Condition |
| --- | --- | --- |
| cell1 | batch1 | wildtype |
| cell2 | batch1 | mutant |
| cell3 | batch2 | wildtype |
| cell4 | batch2 | mutant |

Other columns might be needed, depending on the experimental setup:
* **C1 data** needs a `URL_r1` with the path to the samples fastq file, but no `Barcode` column.
* **Wafergen data** needs a `Barcode` column with the barcode that is used during multiplexing, but no `URL_r1` column.

The names of columns can be arbirtary, but need to be given for the sample name (`index`) and barcode column in the `config/plateseq.yml` file:
```
samplesheet:
  file: SampleSheet.txt
  index: Name
  barcode: Barcode
```

#### Run analysis
A call to `snakemake --configfile <config> --use-conda` will demultiplex barcodes into fastq files, if needed, and quantify the data.

----------------------------------
## Steps used to create the test data

The test data is obtained from a sample of 5000 mouse heart cells. This is one sample, but for testing purposes, we create two artificial samples from two lanes as follows:
```
wget https://cf.10xgenomics.com/samples/cell-exp/7.0.0/5k_mouse_heart_CNIK_3pv3/5k_mouse_heart_CNIK_3pv3_fastqs.tar
tar -xvf 5k_mouse_heart_CNIK_3pv3_fastqs.tar
mkdir test_data
cd 5k_mouse_heart_CNIK_3pv3_fastqs
zcat 5k_mouse_heart_CNIK_3pv3_S3_L001_R1_001.fastq.gz | head -n 400000 | gzip > ../test_data/sample1_R1.fastq.gz
zcat 5k_mouse_heart_CNIK_3pv3_S3_L001_R2_001.fastq.gz | head -n 400000 | gzip > ../test_data/sample1_R2.fastq.gz
zcat 5k_mouse_heart_CNIK_3pv3_S3_L002_R1_001.fastq.gz | head -n 400000 | gzip > ../test_data/sample2_R1.fastq.gz
zcat 5k_mouse_heart_CNIK_3pv3_S3_L002_R2_001.fastq.gz | head -n 400000 | gzip > ../test_data/sample2_R2.fastq.gz
```
