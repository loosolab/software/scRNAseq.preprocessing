#!/usr/bin/env python

from scipy import sparse
import pandas as pd
import anndata
import scanpy
from scipy.io import mmread

#
# Read in as individual adata files, then merge
#
# https://github.com/alexdobin/STAR/issues/774#issuecomment-850477636
def buildvdata(path_gene, path_velocity, condition, index_name="index"):
    """
    First create a normal adata, then add velocity information.
    """
    
    # load expression matrix
    X = scanpy.read_mtx(f'{path_gene}/matrix.mtx')
    # Transpose counts matrix to have Cells as rows and Genes as cols as expected by AnnData objects
    X = X.X.transpose()

    # load cell ids (barcodes) (gene filtered)
    obs = pd.read_csv(f'{path_gene}/barcodes.tsv',
                      header=None,
                      index_col=0)

    obs.index.name = index_name
    # add condition to index to avoid barcode overlap between conditions
    obs.index = obs.index + '-' + condition

    # load gene ids
    var = pd.read_csv(f'{path_velocity}/filtered/genes.tsv',
                      sep='\t',
                      names=('gene_ids', 'feature_types'),
                      index_col=1)

    # TODO suppress var names not unique warning
    # create anndata object
    adata = anndata.AnnData(X=X, obs=obs, var=var)

    # load velocity filtered barcodes
    vobs = pd.read_csv(f'{path_velocity}/filtered/barcodes.tsv',
                       header = None,
                       index_col = 0)
    # add condition to index to avoid barcode overlap between conditions
    vobs.index = vobs.index + '-' + condition

    # filter adata to match velocity data
    if len(obs) > len(vobs):
        adata = adata[vobs.index.to_list(),:].copy()

    # load & add velocity data
    adata.layers['spliced'] = sparse.csr_matrix(mmread(f'{path_velocity}/filtered/spliced.mtx')).transpose()
    adata.layers['unspliced'] = sparse.csr_matrix(mmread(f'{path_velocity}/filtered/unspliced.mtx')).transpose()
    adata.layers['ambiguous'] = sparse.csr_matrix(mmread(f'{path_velocity}/filtered/ambiguous.mtx')).transpose()

    adata.var_names_make_unique()

    return adata.copy()

adata_list = list()

for path_gene, path_velocity, condition in zip(snakemake.input.gene, snakemake.input.velocity, snakemake.params.samples):
    adata_list.append(buildvdata(path_gene, path_velocity, condition))

# merge anndata list
adata = scanpy.concat(adatas=adata_list, axis=0, label="Sample", join="outer", index_unique="-", merge="same", keys=snakemake.params.samples)

# Merge metadata if present
if snakemake.params.metadata.shape[1] > 0:
    adata.obs = adata.obs.reset_index().merge(snakemake.params.metadata, how = 'left', left_on='Sample', right_on='Sample').set_index('index')

# Add git commit to internal metadata
adata.uns['scRNAseq.preprocessing'] = {'version': snakemake.params.git_commit}

# Save object to file
adata.write(filename=snakemake.output.anndata, compression='gzip')
