#!/usr/bin/env python
import scanpy
import pandas as pd
from pathlib import Path

#
# Read in as individual anndata files, then merge
#
anndata_list = list()

# iterate over cite-seq output folders
#
# https://hoohm.github.io/CITE-seq-Count/Reading-the-output/
# use umi_count/ or read_count/
# umi_count is recommended for analysis
# read_count is for checking overamplification or oversequencing issues
subdir = "umi_count"
# subdir contains:
#     features.tsv.gz = e.g. antibody tag names. Saved in anndata.var
#     barcodes.tsv.gz = cell barcode. Saved in anndata.obs
#     matrix.mtx.gz   = Matrix contains count values. Saved in anndata.x

#import pdb; pdb.set_trace() # uncomment for interactive debugging

for folder in snakemake.input:
    # create path object
    path = Path(folder)

    # load files into anndata
    anndata = scanpy.read_mtx(filename=path / subdir / "matrix.mtx.gz").T
    anndata.var = pd.read_csv(filepath_or_buffer=path / subdir / "features.tsv.gz", header=None, index_col=0, sep="\t", names=["Hashes"])
    anndata.obs = pd.read_csv(filepath_or_buffer=path / subdir / "barcodes.tsv.gz", header=None, index_col=0, sep="\t", names=["Barcodes"])

    # get sample from path and add to adata.obs
    anndata.obs["Sample"] = path.parents[2].name

    # remove "unmapped" feature
    anndata = anndata[:,~(anndata.var.index == "unmapped")]

    # add to list
    anndata_list.append(anndata)

# merge anndata list
anndata_hash = scanpy.concat(adatas=anndata_list, axis=0, label="citeseq_run", join="outer", index_unique="-", merge="same")

# split feature index by #. tag-id, protein, tag-barcode
anndata_hash.var[["TagId", "Protein", "TagBarcode"]] = anndata_hash.var.index.str.split(pat="#", n=3, expand=True).tolist()

# write anndata
anndata_hash.write(filename=snakemake.output[0], compression='gzip')
