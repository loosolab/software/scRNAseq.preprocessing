#!/usr/bin/env python

import numpy as np
import pandas as pd
import anndata as ad

# Generate list of quantified transcripts
tx = pd.read_csv(snakemake.input.quant_files[0], sep='\t', usecols=[0])

# Read tx2gene mapping
tx2gene = pd.read_csv(snakemake.input.tx2gene[0], sep='\t', header=None, names=[
                      'transcript', 'gene', 'gene_type'], index_col='transcript')

# Subset to quantified transcripts
tx2gene = tx2gene.loc[tx2gene.index.isin(tx.Name)]

# Generate a unique set of genes
genes = list(set(tx2gene.gene))

# Generate bins for numpy.bincount
gene_bins = [genes.index(s) for s in tx2gene.loc[tx.Name].gene]

# Read transcript abundance and quantify genes
counts = list()
tpm = list()
for file in snakemake.input.quant_files:
    q = np.loadtxt(fname=file, usecols=[3, 4], skiprows=1).T
    counts.append(np.bincount(gene_bins, weights=q[1, ]))
    tpm.append(np.bincount(gene_bins, weights=q[0, ]))
counts = np.stack(counts)
tpm = np.stack(tpm)

# Filter genes not expressed at all
use_genes = np.nonzero(np.sum(counts, axis=0))[0]
genes = [genes[i] for i in use_genes]
counts = counts[:, np.nonzero(np.sum(counts, axis=0))[0]]
tpm = tpm[:, np.nonzero(np.sum(counts, axis=0))[0]]

# Create obs df
if len(snakemake.input.metadata) > 1:
    colData = snakemake.params.colData

    # First entry is Samplesheet, we already have it.
    for file in snakemake.input.metadata[1:]:
        obs_df = pd.read_csv(file, header = 0, sep = '\t')
        
        colData = pd.merge(colData, obs_df, how = "left", left_on = snakemake.config['samplesheet']['index'], right_on = snakemake.config['samplesheet']['index'])
else:
    colData = snakemake.params.colData

# Create var df
rowData = pd.DataFrame(data = {'gene': genes}, index = genes)

# Annotate genes
if snakemake.config['action']['annotation']:
    annotation_df = pd.read_csv(snakemake.input.annotation[0], header=0, sep='\t')

    # Left join gene list with annotation, fill missing annotations with NA
    rowData = pd.merge(rowData, annotation_df, how = "left", left_on = 'gene', right_on = snakemake.config['annotation']['filter'])
    rowData = rowData.set_index('gene')

adata = ad.AnnData(counts, layers={'counts': counts, 'tpm': tpm}, obs=colData, var=rowData, uns={
                   'scRNAseq.preprocessing': {'version': snakemake.params.git_commit}})

adata.write(filename=snakemake.output.anndata, compression='gzip')
