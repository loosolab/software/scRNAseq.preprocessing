#!/usr/bin/env python

import pandas as pd
from bioservices import BioMart

# Read reference
reference = pd.read_csv(snakemake.input[0], sep = '\t', header=None, names = ['transcript', 'gene', 'gene_type'])
genes = reference['gene'].unique()

# Assemble filter and attributes from config
attributes = snakemake.config['annotation']['attributes']

if not snakemake.config['annotation']['filter'] in attributes:
  attributes = [snakemake.config['annotation']['filter']] + snakemake.config['annotation']['attributes']

# Assemble query
bm = BioMart(host = "{}.ensembl.org".format(snakemake.config['annotation']['mirror']))
bm.add_dataset_to_xml(snakemake.config['annotation']['dataset'])
bm.add_filter_to_xml(snakemake.config['annotation']['filter'], ','.join(genes))

for attribute in attributes:
  bm.add_attribute_to_xml(attribute)
    
annotation_query = bm.get_xml()
res = bm.query(annotation_query)

# Reshape results in a pd.DataFrame
annotation = list()
for entry in res.split('\n'):
  if entry != '':
    annotation.append(dict(zip(attributes, entry.split('\t'))))
    
annotation_df = pd.DataFrame(annotation)
annotation_df.to_csv(path_or_buf = snakemake.output[0], sep = '\t', index = False, compression = None)
