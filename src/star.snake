# vim: syntax=python tabstop=4 expandtab
# coding: utf-8

'''
@author: jpreuss

Provides mapping and counting of 10X data
'''

def star_input(wildcards):
  bc = switch_input_s3(SAMPLES[wildcards.sample]['URL_r2'])
  cdna = switch_input_s3(SAMPLES[wildcards.sample]['URL_r1'])
  if config['action']['clean']:
    cdna = rules.htstream_clean_se_gz.output
  return({'bc': bc, 'cdna': cdna})

rule star_map:
  input: unpack(star_input)
  output:
    bam = config['dirs']['quant'] + '/{sample}/Aligned.sortedByCoord.out.bam',
    gene = directory(config['dirs']['quant'] + '/{sample}/solo/Gene/filtered/'),
    gene_raw = directory(config['dirs']['quant'] + '/{sample}/solo/Gene/raw/'),
    velocity = directory(config['dirs']['quant'] + '/{sample}/solo/Velocyto/'),
    geneFull = directory(config['dirs']['quant'] + '/{sample}/solo/GeneFull/filtered'),
    stats = config['dirs']['quant'] + '/{sample}/solo/Gene/Summary.csv'
  params:
    index = config['star']['index'] if 'index' in config.get('star', {}) else '',
    whitelist = config['star']['whitelist'] if 'whitelist' in config.get('star', {}) else '',
    flags = config['star']['solo_flags'] if 'solo_flags' in config.get('star', {}) else '',
    cellfilter = config['star']['cellfilter'], 
  conda: "../envs/star.yml"
  log: config['dirs']['log']+'/star/{sample}.log'
  threads: 16
  message: "Mapping and counting of sample {wildcards.sample} using STAR-solo."
  shell:
    "mkdir -p {config[dirs][quant]}/{wildcards.sample}; "
    "STAR --runMode alignReads --runThreadN {threads} --outFileNamePrefix {config[dirs][quant]}/{wildcards.sample}/ --genomeDir {params.index} "
    "--readFilesIn {input.cdna} {input.bc} --readFilesCommand zcat --outSAMtype BAM SortedByCoordinate --soloOutFileNames solo/ genes.tsv barcodes.tsv matrix.mtx "
    "--soloCBwhitelist {params.whitelist} --soloFeatures Gene Velocyto GeneFull {params.cellfilter} {params.flags} > {log}; "
    "cat {config[dirs][quant]}/{wildcards.sample}/Log.out >> {log}"

rule star_stats:
  input: expand('{dir}/{sample}/solo/Gene/Summary.csv', dir = config['dirs']['quant'], sample = SAMPLE_NAMES)
  output:
    table = config['dirs']['tables'] + '/starsolo.txt'
  params:
    samples = SAMPLE_NAMES,
    label = 'Sample'
  message: 'Collecting statistics from STARsolo runs.'
  run:
    import pandas as pd

    stats = list()

    for file in input:
      with open(file, "r") as f:
        data = pd.read_csv(f, header = None, names = ['Stat', 'Value'])

      sample_row = dict(zip(data['Stat'], data['Value']))
      stats.append(sample_row)

    df = pd.DataFrame(stats, index = params.samples)
    df.to_csv(output.table, sep = "\t", index_label = params.label)
