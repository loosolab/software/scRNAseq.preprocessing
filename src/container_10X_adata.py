#!/usr/bin/env python

import pandas
import scanpy

#
# Read in as individual adata files, then merge
#
adata_list = list()
for d in snakemake.input.dirs:
    adata_list.append(scanpy.read_10x_mtx(d))

# merge anndata list
adata = scanpy.concat(adatas=adata_list, axis=0, label="Sample", join="outer", index_unique="-", merge="same", keys=snakemake.params.samples)

# Filter genes not expressed at all
if snakemake.config['container']['exclude_zeros']:
    scanpy.pp.filter_genes(adata, min_cells = 1)

# Merge metadata if present
if snakemake.params.metadata.shape[1] > 0:
    adata.obs = adata.obs.reset_index().merge(snakemake.params.metadata, how = 'left', left_on='Sample', right_on='Sample').set_index('index')

# Add git commit to internal metadata
adata.uns['scRNAseq.preprocessing'] = {'version': snakemake.params.git_commit}

# Save object to file
adata.write(filename=snakemake.output.anndata, compression='gzip')
