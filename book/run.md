# Running the workflow

The workflow can be run by calling `snakemake` and passing the workflows `Snakefile` along with the configuration file:

```
snakemake -s scRNAseq.preprocessing/Snakefile --configfile config/default.yml
```

#### Popular options for snakemake

Option | Effect
------ | ------
`-n` | Initiate a *dry-run*. This simulates the workflow.
`-r` | Print the reason for each rule execution.
`-p` | Print the command for each rule execution.
`--use-conda` | Uses the conda package manager to pull in dependencies.
`--conda-prefix /tmp` | Will save newly created conda envs in `/tmp`.

Check also other [command line parameters of snakemake](https://snakemake.readthedocs.io/en/stable/executing/cli.html).

