# Getting started with Hash antibody's

Cell Hashing is a method that enables sample multiplexing and super-loading on single cell RNA-sequencing platforms, developed in the Technology Innovation lab at the New York Genome Center in collaboration with the Satija lab.
Cell Hashing uses a series of oligo-tagged antibodies against ubiquitously expressed surface proteins with different barcodes to uniquely label cells from distinct samples, which can be subsequently pooled in one scRNA-seq run. By sequencing these tags alongside the cellular transcriptome, we can assign each cell to its sample of origin, and robustly identify doublets originating from multiple samples. [source](https://cite-seq.com/cell-hashing/), [Rahul-Satija](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-018-1603-1)

![image](/uploads/84099eecbaca109e46a650732ce4d2d7/image.png)

# Demultiplexing 

The demultiplexing is done by [CITE-seq](https://hoohm.github.io/CITE-seq-Count/) inside the piline.
To run CITE-seq-Count, as it is done in the pipline, all Hash reads must have the same length. The Adapter trimming produces sometimes reads which have one base more so i trimmed them all to same length with fastx_trimmer to the standard length of 28bp. 

`
fastx_trimmer -z -l 28 -i Voss_Lib3_Hash_R2.fastq -o Voss_Lib3_Hash_R2_trim.fastq.gz
`

**CITE-seq-Count**
CITE-seq source "https://hoohm.github.io/CITE-seq-Count/" Instaltion `pip install CITE-seq-Count==1.4.4`

**Example**
`
CITE-seq-Count -R1  raw/Voss_Lib3_Hash_R1.fastq.gz -R2 raw/Voss_Lib3_Hash_R2_trim.fastq.gz --bc_collapsing_dist 2  -t reference_citeseq.csv -cells 0 -wl barcodes.tsv  -cbf 1 -cbl 16 -umif 17 -umil  28 --threads 16 --sliding-window
`

* -R1: Barcode file 
* -R2: Hash file 
* -cbf 1 : Start of Barcode
* -cbl 16 : End of Barcode
* -umif 17: start of umi
* -umil  28 end of umi 
* --bc_collapsing_dist 2  mistakes allowed in barcode (important set to two to get meaningful results lower values will produce low hash counts which cloud not be specified to a hash.)
* --sliding-window searches for best matching position of Hash. Need to be set as some hashes are not on fixed position. The hash reads start at bp 10 and are 15 bp long in normale.
* -wl barcodes whitelist comes from STARsolo demultiplexing.

**Output**
Citeseq creates two count matrices
* Count_full/
* ---read_count/ Count of reads without umi correction
* ---umi_count/  Counts with umi correction (**needed for hash detection**)

## Ongoing analysis in Jupyter notebook

The steps above are done by the pipline you find the output of Citeseq as adata object in `data/hash_adata.h5ad`.
