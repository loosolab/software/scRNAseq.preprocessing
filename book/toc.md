# Summary

* [Preface](preface.md)
* [Setup](preprocessing/setup.md)

### Part 1: Preprocessing
 
1. [Preparing the input](preprocessing/input.md)
  1. [Plate-based sequencing](preprocessing/plate.md)
  2. [10X Genomics Chromium](preprocessing/10X.md) 
2. Preprocessing steps
  1. [Genomic reference](preprocessing/reference.md)
  2. [Cleaning of input files](preprocessing/cleaning.md)
  3. [Alignment](preprocessing/alignment.md)
  4. [Gene annotation](preprocessing/annotation.md)
  6. [Data container](preprocessing/container.md)
3. [Running the pipeline](run.md)
4. Output files
5. Error diagnostics
6. Obtaining updates
7. Frequently asked questions

### Part 2: Analysis

* Python
  * [Getting started](analysis/python-getting-started.md)
  * [Hash Analysis](https://gitlab.gwdg.de/loosolab/software/scRNAseq.preprocessing/-/blob/master/book/analysis/hash_analysis.ipynb)

  
* R/RStudio

### Part 3: Advanced topics

1. Development
