# Preparing the workflow input

The workflow features input from two types of sequencing technology (*plate-based single-cell RNA-sequencing* and *10X Genomics Chromium Single Cell Gene Expression*) but typically requires three inputs to run: 

1. A **configuration file** that contains parameters and settings of individual workflow steps.
2. Information on the **samples** being preprocessed. 
3. The actual **input files** themselves, containing raw sequencing output.

## The configuration file

The configuration file is a **YAML file** that contains parameters and settings for the workflow. The source distribution contains configuration file templates for both sequencing technologies.

#### Customizing the folder structure {#input-custom-folder}

By default, the workflow creates a folder structure for its various output and log files. To customize the locations of those, find the `dirs` section in the configuration file and change the values accordingly.

## Sample information


## Input files

