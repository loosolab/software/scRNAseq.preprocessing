# Plate-based single-cell sequencing

## Obtaining the default configuartion file

A template can be found in the source distributions [config/plate.yml](https://gitlab.gwdg.de/jens.preussner/scRNAseq.preprocessing/blob/master/config/plate.yml) file and should be copied to the working directory before customization.

## Preparing the sample information

Sample information for plate-based single-cell sequencing is gathered in a samplesheet.

The samplesheet is a **tab-separated** text file with a header line that contains at least one (*index*) column and one row per sample. The *index* column can be named arbitrary and it should contain unique sample identifier (*sample names*). In addition to the *index* column, the samplesheet can contain a column named `URL_r1`, with absolute or relative paths to sample input files (see [below](#input-input-files)). The samplesheet can additionally contain arbitrary [metadata columns](#metadata).

**Important** If no `URL_r1` column is present, the workflow will search for sample input files by pasting (1) the path of the default input folder, (2) their *sample name* and (3) the file ending `.fastq.gz` (see [below](#input-hard-disk)). 

#### Examples

1. A samplesheet with the *index* column named `Sample`, and a `URL_r1` column with relative file paths:

```
Sample  URL_r1
sample1 fastq/sample_a.fastq.gz
sample2 fastq/sample_b.fastq.gz
```

2. A samplesheet with the *index* column named `Index`, and two *metadata* columns. Note that in this case, values of `URL_r1` will be `fastq/sample1.fastq.gz` and `fastq/sample2.fastq.gz`.

```
Index  Type_A  Type_B
sample1 tA  M1
sample2 tB  M1
```

#### Customizing the samplesheet

By default, the samplesheet is expected to be named `Samplesheet.txt` and contain an *index* column called `Sample` (case sensitive!). 

Find the `samplesheet` section in the configuration file and change

- the value of `file` to change the default name and location of the samplesheet,
- the value of `index` to change the default name of the *index* column.

## Sample input files {#input-input-files}

Currently, the workflow expects a **single gzipped fastq file** per sample. These files can be located on hard disk, network shares or in a remote S3 bucket:

#### Input from files on hard disks or network drives {#input-hard-disk}

Input from files on local hard disk or network drives is the default. You can use the `URL_r1` column in the samplesheet to specify a custom location for each sample. If no `URL_r1` column is present in the samplesheet, the workflow will search for sample input files by pasting (1) the path of the default input folder[^1], (2) their *sample name* from the *index* column and (3) the file ending `.fastq.gz`. To change the default input folder, [customize the folder structure](preprocessing/input.md#input-custom-folder) and change the value of `fastq` to a custom location.

#### Input from a remote S3 bucket

Find the `s3` section in the configuration file and customize/provide the following information:

- `endpoint`, the URL of the S3 endpoint
- `bucket`, the name of the S3 bucket containing the input data
- `access_key_id`, the access key ID
- `secret_access_key`, the secret access key

Additionally, find the `action` section of the configuration file and enable S3 input by setting `s3: True`. 

[^1]: By default, input files are searched in a folder called `fastq`.