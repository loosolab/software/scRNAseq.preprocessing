# Cleaning of input fastq files

Quality control of input files is facilitated using different modules from [HTStream](https://ibest.github.io/HTStream/). Please see their website for reference.

To customize the sequence of [HTStream](https://ibest.github.io/HTStream/) actions, find the `htstream` section in the configuration file and change the value of `actions` (without the `hts_` prefix). To customize command line parameters of each action, you can additionally modify the value of `flags`. 

**Important** The order of items in `actions` and `flags` has to match.

## Obtaining statistics from HTStream

The workflow includes logic to extract *statistics* from HTStream log files. To obtain such *statistics*, use the value of `collect`. 

**Note** Some of the statistics have the same name across different actions. Make use of the `--notes` parameter of HTStream actions to annotate the origin of statistics from that action.
