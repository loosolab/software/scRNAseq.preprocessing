# Workflow requirements and setup

## System requirements

In order to run the workflow and preprocess raw sequencing data, you'll need:

* a 32- or 64-bit computer
* a Linux operating system like [Debian](debian.org), [CentOS](centos.org) or [Ubuntu](ubuntu.com)
* a suitable amount of free disk space, e.g. 20 GB
* Python 2.7, 3.4, 3.5 or 3.6.
* Git (optional, but recommended)

The workflow has two main software requirements: The **conda** package manager and **Snakemake**, the workflow scheduler.

### Fast steup 

Clone the pipeline and create the env from the yml file.

```
git clone git@gitlab.gwdg.de:loosolab/software/scRNAseq.preprocessing.git
cd scRNAseq.preprocessing
conda env create -f environment.yml
conda activate snakemake
```


### Conda installation and configuration

The fastest way to obtain `conda` is to [install Miniconda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). 
After the installation is complete, conda needs to be **reconfigured** to include two important channels for additional software:

```bash
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```

#### Installation of snakemake

The workflow scheduler can be obtained via `conda` as well:

```bash
conda install snakemake
```

#### Recommended: Install git

To easily obtain the workflow and have the possibility to upgrade it later, we recommend to install `git` as well:

```bash
conda install git
```

## Workflow setup

It is best practise to use a *working directory*, where the preprocessing workflow can write result files. Personally, I prefer to keep the preprocessing workflow in that directory as well, but in principle it does not matter where the workflow resides on your disk. 

#### 1. Create a working directory 

```bash
mkdir -p /path/to/working_directory
```

#### 2a. Setup the workflow via git submodules (preferred)

```bash
cd /path/to/working_directory
git init
git submodule add https://gitlab.gwdg.de/jens.preussner/scRNAseq.preprocessing.git
```

#### 2b. Setup the workflow somewhere else (alternative)

The worklflow can be setup at any location on your computer, either with git (top) or without (below).

**Using git** to checkout a *specific* version:

```bash
cd /path/to/software
version="v0.5"
git checkout https://gitlab.gwdg.de/jens.preussner/scRNAseq.preprocessing.git
git checkout ${version}
```

**Using git** to checkout the *development* version:

```bash
cd /path/to/software
git checkout https://gitlab.gwdg.de/jens.preussner/scRNAseq.preprocessing.git
```

**Using wget** (or any other download method) to download a specific version:

```bash
cd /path/to/software
version="v0.5"
wget https://gitlab.gwdg.de/jens.preussner/scRNAseq.preprocessing/-/archive/${version}/scRNAseq.preprocessing-${version}.tar.gz
tar xfvz scRNAseq.preprocessing-${version}.tar.gz
```

Please check [available tags](https://gitlab.gwdg.de/jens.preussner/scRNAseq.preprocessing/-/tags) for the latest version.
