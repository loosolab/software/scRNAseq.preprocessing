# Preparing the reference

The reference is expected to be one or more **fasta file(s)**, containing either genomic or transcriptomic sequences of interest. To change the default location of the reference, find the `reference` section in the configuration file and change the value of `URL`.

## Using pre-build indices

To make use of pre-build genome or transcriptome indices, the path to the index can be inserted using `index: /path/to/index`. Pre-build indices can not be used together with [spike-ins](#using-spike-ins).

## Using a local reference

Sometimes, a transcriptome reference from a local (non-remote) location is desired to be used. If the `URL` is not poiting to an *FTP* server, change `protocol: ftp` to `protocol: none`.

## Using remote references

By default, a transcriptome reference is fetched automatically from a [Gencode release](https://www.gencodegenes.org/). To allow easy customization of *which* reference exactly to use, the default `URL` contains **wildcards**, which are filled from the `reference` section during runtime. The **wildcards** are explained below:

- **organism** - This can be `mouse` or `human` for Gencode transcriptome references.
- **release** - The Gencode release number.
- **file** - Possibly multiple files that make up the transcriptome reference.

## Using spike-ins {#using-spike-ins}

Some experiments make use of genetically modified or labelled organisms. The workflow allows to insert such *transgenes* into the transcriptome reference *on the fly* and quantify their gene expression as well.

To do so, insert the path to a fasta file containing the transgene(s) cDNA sequences using `spikeIns: /path/to/transgenes.fasta`. 
