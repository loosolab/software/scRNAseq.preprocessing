# Preface

> The goal is to turn data into information, and information into insight.
>
> Carly Fiorina, *former CEO of HP*

Having the above quote in mind, the goal of data preprocessing from single-cell RNA-seq experiments aims to convert raw sequencing data to information that can easily be used for data analysis. 

This book describes a workflow that covers various preprocessing steps and serves as a manual for bioinformaticians and as a guide for interested biologists willing to learn how to take preprocessed information and create insight. It is divided into two parts, each targeting a different audience:

* **Part 1** describes the preprocessing workflow for technically experienced readers. It aims to enable readers to conduct preprocessing from single-cell RNA-seq experiments on their own.
* **Part 2** contains a guide on how to analyse information generated in part 1 and possible generate insight from it. 

