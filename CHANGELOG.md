# Changelog

## 09-02-2024
- add barcode tag to bam file

## 10-11-2023
- fix plate mode
- temporarily disabled R containers for plate

## 02-11-2023
- fix gene id not in adata.var

## 01-11-2023
- pandas version to != 2.1.2, https://github.com/scverse/anndata/issues/1210

## 02-08-2023
- add star cellfilter options (CellRanger2.2 or EmptyDrops_CR)

## 27-07-2023
- added option for FullGene mapping (exons and **introns**)

## 30-03-2023
- Added separate test data config
- Moved test data and test configs to tests/

## 09-03-2023
- Addition of checks for missing sample keys in config
