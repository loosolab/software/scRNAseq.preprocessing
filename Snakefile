import pandas as pd
from os.path import join, basename, dirname

snakemake.utils.min_version("5.7")

if workflow.overwrite_configfiles != None:
	configfiles: workflow.overwrite_configfiles
else:
	configfiles: ['config/default.yml']

#-------------------------------------------------------------------------------#
#------------- Read samplesheet and provide necessary variables ----------------#
#-------------------------------------------------------------------------------#

if config['action']['mode'] == '10X':
	# Gather metadata 
	samplesheet = pd.DataFrame.from_dict(config['data'], orient = "index")
	samplesheet.index.name = 'Sample'

	# Construct URLs to sample FASTQ files (lane awareness)
	samplesheet['URL_r1'] = ['{dir}/{sample}_R1.{format}'.format(dir = config['dirs']['fastq'], sample = sample, format = 'fastq.gz') for sample in list(samplesheet.index)]
	samplesheet['URL_r2'] = ['{dir}/{sample}_R2.{format}'.format(dir = config['dirs']['fastq'], sample = sample, format = 'fastq.gz') for sample in list(samplesheet.index)]

	is_PE = False

if config['action']['mode'] == 'plate':
	samplesheet = pd.read_csv(config['samplesheet']['file'], sep = '\t', index_col = config['samplesheet']['index'])

	is_PE = ('URL_r2' in list(samplesheet))

	if 'URL_r1' not in list(samplesheet):
		samplesheet['URL_r1'] = ['{dir}/{sample}.{format}'.format(dir = config['dirs']['fastq'], sample = sample, format = 'fastq.gz') for sample in list(samplesheet.index)]

SAMPLES = samplesheet.to_dict(orient = 'index')
SAMPLE_NAMES = sorted(SAMPLES.keys())

# check for missing values. Each sample has to have the same keys!
if samplesheet.isna().values.any():
	na_cols = list(samplesheet.columns[samplesheet.isnull().any()])
	
	raise ValueError(f"It is mandatory for all samples to have the same entries! Found {na_cols} not present for all samples. Check for typos in your config file.")

#-------------------------------------------------------------------------------#
#--------------------------------- Handle S3 -----------------------------------#
#-------------------------------------------------------------------------------#
if config['action']['s3']:
	from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider
	S3 = S3RemoteProvider(host = config['s3']['endpoint'], access_key_id = config['s3']['access_key_id'], secret_access_key = config['s3']['secret_access_key'])

#-------------------------------------------------------------------------------#
#--------------------------- Generate output files -----------------------------#
#-------------------------------------------------------------------------------#

if config['action']['mode'] == 'plate':
	output_files = [
		join(config['dirs']['ref'], 'tx2gene')
	]
elif config['action']['mode'] == '10X':
	output_files = []

if config['action']['clean']:
	clean_files = expand('{o}/{s}.clean.fastq.gz', o = config['dirs']['fastq'], s = SAMPLE_NAMES)
	output_files.extend(clean_files)
	output_files.extend([join(config['dirs']['tables'], 'htstream.txt')])

	if is_PE:
		clean_files_pe = expand('{o}/{s}_r2.clean.fastq.gz', o = config['dirs']['fastq'], s = SAMPLE_NAMES)
		output_files.extend(clean_files_pe)

if config['action']['mode'] == 'plate':
	quant_files = expand('{o}/{s}/quant.sf', o = config['dirs']['quant'], s = SAMPLE_NAMES)
	output_files.extend([join(config['dirs']['tables'], 'salmon.txt')])

	if config['action']['velocity']: 
		output_files.append(join(config['dirs']['ref'], 'ix2gene'))
		# Handle mappings to the intron for RNA velocity
		iquant_files = expand('{o}/{s}/quant.sf', o = config['dirs']['iquant'], s = SAMPLE_NAMES)
		output_files.extend(iquant_files)
elif config['action']['mode'] == '10X':
	quant_files = expand('{o}/{s}/Aligned.sortedByCoord.out.bam', o = config['dirs']['quant'], s = SAMPLE_NAMES)
	output_files.extend([join(config['dirs']['tables'], 'starsolo.txt')])

output_files.extend(quant_files)

if config['action']['container']:
	output_files.extend([join(config['dirs']['container'], 'adata.h5ad')])

	# TODO fix R containers for plate mode
	if config['action']['mode'] != 'plate':
		output_files.extend([join(config['dirs']['container'], 'SCE.rds'), join(config['dirs']['container'], 'seurat.rds')])

	if config['action']['velocity']:
		output_files.append(join(config['dirs']['container'], 'vdata.h5ad'))

	if config['action']['GeneFull']:
		output_files.append(join(config['dirs']['container'], 'gfdata.h5ad'))
		
#add hash output
if config['action']['hash']:
	output_files.append(join(config['dirs']['container'], 'hash_adata_filtered.h5ad'))

	if config['hash_options']['raw']:
		output_files.append(join(config['dirs']['container'], 'hash_adata_raw.h5ad'))

	CONDITIONS = list(config['hash_data'].keys())
	SAMPELS_HASH = config['hash_data']

#-------------------------------------------------------------------------------#
#------------------------------ Handle options ---------------------------------#
#-------------------------------------------------------------------------------#

if config['action']['mode'] == '10X':
	if config['star']['cellfilter_type'] == "CellRanger2.2":
		cellfilter_list = [config['star'].get('cellfilter_type', "CellRanger2.2"),
						config['star'].get('cellfilter_ExpectedCells', 3000),
						config['star'].get('cellfilter_maxPercentile', 0.99),
						config['star'].get('cellfilter_maxMinRatio', 10)]
		cellfilter_list = [str(i) for i in cellfilter_list]

		config['star']['cellfilter'] = '--soloCellFilter ' + ' '.join(cellfilter_list)  # final cellfilter parameters
	elif config['star']['cellfilter_type'] == "EmptyDrops_CR":
		cellfilter_list = [config['star'].get('cellfilter_type', "EmptyDrops_CR"),
						config['star'].get('cellfilter_ExpectedCells', 3000),
						config['star'].get('cellfilter_maxPercentile', 0.99),
						config['star'].get('cellfilter_maxMinRatio', 10),
						config['star'].get('cellfilter_indMin', 45000),
						config['star'].get('cellfilter_indMax', 90000),
						config['star'].get('cellfilter_umiMin', 500),
						config['star'].get('cellfilter_umiMinFracMedian', 0.01),
						config['star'].get('cellfilter_candMaxN', 20000),
						config['star'].get('cellfilter_FDR', 0.01),
						config['star'].get('cellfilter_simN', 10000)]
		cellfilter_list = [str(i) for i in cellfilter_list]

		config['star']['cellfilter'] = '--soloCellFilter ' + ' '.join(cellfilter_list)  # final cellfilter parameters

#-------------------------------------------------------------------------------#
#---------------------------------- RUN :-) ------------------------------------#
#-------------------------------------------------------------------------------#

include: "src/auxiliary.snake"

if config['action']['mode'] == 'plate':
	include: "src/reference.snake"
	include: "src/annotate.snake"

include: "src/clean.snake"

if config['action']['mode'] == 'plate':
	include: "src/salmon.snake"
	include: "src/container_plate.snake"
elif config['action']['mode'] == '10X':
	include: "src/fastq.snake"
	include: "src/star.snake"
	include: "src/container_10X.snake"
	if config['action']['hash']:
		include: "src/hash.snake"

if config["debug"]:
  print_debug()

rule all:
  input:
    output_files
  message: "Done."
